import axios from 'axios';
import { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';

const URI = 'http://localhost:4000/';
const ENDPOINT_VER_SERVICIO = 'servicio/';

const ComponenteServicioVer = () => {

  const [servicio, setServicio] = useState([]);
  const navigate = useNavigate();
  const {id} = useParams();
  

  useEffect(()=> {
    obtenerServicio(id);
  },[]);

  const obtenerServicio = async(id) => {
    const respuesta = await axios.get(URI+ENDPOINT_VER_SERVICIO+id);
    setServicio(respuesta.data.servicio);
  }

  function formatoAdicionalServicio(adicional){
    if(adicional === ''){
      return 'Ninguno';
    }else{
      return adicional;
    }
  }

  function formatoFinalizado(finalizado){
    if(finalizado){
      return 'Sí';
    }else{
      return 'NO';
    }
  }

  const convertirMayus = () => {
    this.toUpperCase();
  }

  

  return(
    <>
    <h1 className='text-center mt-4'>DETALLE DEL SERVICIO</h1>
    <ul className='ms-5 fs-4 mt-4'>
      <li>Tipo de Servicio = <span className='text-uppercase fs-3'>{servicio.tipoServicio}</span></li>
      <li>C.C Solicitante = <span className='fs-3'>{servicio.documentoUsuario}</span></li>
      <li>Adicional = <span className='fs-3 text-uppercase'>{formatoAdicionalServicio(servicio.adicional)}</span></li>
      <li>Ticket = <span className='fs-3'>{servicio.idTicket}</span></li>
      <li>¿Servicio Finalizado? = <span className='fs-3 text-uppercase'>{formatoFinalizado(servicio.finalizado)}</span></li>
      <li>Fecha del servicio = <span className='fs-3'>{servicio.fecha}</span></li>
      <li>Hora del servicio = <span className='fs-3'>{servicio.hora}</span></li>
    </ul>
    <hr></hr>
    <Link to={`/`}>
      <button type="button" className="btn btn-primary fs-4 ms-5">
        <i className="fa-solid fa-arrow-left"></i> Volver
      </button> 
    </Link>
    </>
  )
}

export default ComponenteServicioVer;