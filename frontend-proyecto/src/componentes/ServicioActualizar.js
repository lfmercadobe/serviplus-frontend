import axios from 'axios';
import { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';

const URI = 'http://localhost:4000/';
const ENDPOINT_ACTUALIZAR_SERVICIO = 'actualizarServicio/';
const ENDPOINT_VER_SERVICIO = 'servicio/';
const END_POINT_TIPOS_SERVICIOS = 'tipoServicios';

const ComponenteActualizarServicio = () => {

  const [tipoServicioInput, setTipoServicio] = useState('');
  const [documentoUsuarioInput, setDocumentoUsuario] = useState('');
  const [adicionalInput, setAdicional] = useState('');
  const [finalizadoInput, setFinalizado] = useState('');
  const [fechaInput, setFecha] = useState('');
  const [horaInput, setHora] = useState('');
  const [tipoServiciosDB, setTiposServiciosDB] = useState([]);

  const manejadorFinalizado = () => {
    setFinalizado(!finalizadoInput);
  };

  const navigate = useNavigate();
  const {id} = useParams();

  const actualizarServicio = async(event) => {
    event.preventDefault();
    await axios.put(URI+ENDPOINT_ACTUALIZAR_SERVICIO+id,{
      tipoServicio:tipoServicioInput,
      documentoUsuario:documentoUsuarioInput,
      adicional:adicionalInput,
      finalizado:finalizadoInput,
      fecha:fechaInput,
      hora:horaInput
    });
    navigate('/')
  }

  useEffect(()=>{
    buscarServicioPorId();
    obtenerTiposServicios();
  },[]);

  const obtenerTiposServicios = async()=>{
    const respuesta = await axios.get(URI+END_POINT_TIPOS_SERVICIOS);
    let nombresServicios = respuesta.data.TipoServicios.map((tipoServicio) => tipoServicio.nombre);
    setTiposServiciosDB(nombresServicios);
  }

  const buscarServicioPorId = async()=>{
    const respuesta = await axios.get(URI+ENDPOINT_VER_SERVICIO+id);
    setTipoServicio(respuesta.data.servicio.tipoServicio);
    setDocumentoUsuario(respuesta.data.servicio.documentoUsuario);
    setAdicional(respuesta.data.servicio.adicional);
    setFinalizado(respuesta.data.servicio.finalizado);
    setFecha(respuesta.data.servicio.fecha);
    setHora(respuesta.data.servicio.hora);
  }



  return(
    <>
    <h1 className='text-center m-4'>ACTUALIZAR SERVICIO</h1>
    <div className='container'>
      <div className='row'>
        <div className='col-4'></div>
        <div className='col-4 fs-3'>
          <form onSubmit={actualizarServicio}>
            <div className="mb-3">
              <label htmlFor="tipoServicio" className="form-label">Tipo de Servicio:</label>
              <select value={tipoServicioInput} className="form-select fs-5" id="selectServicio" onChange={(event)=>setTipoServicio(event.target.value)}>
              {tipoServiciosDB.map(tipo => (
              <option key={tipo} value={tipo}>{tipo}</option>
              ))}
              </select>
            </div>
            <div className="mb-3">
              <label htmlFor="documentoUsuario" className="form-label">Documento Usuario:</label>
              <input type="text" value={documentoUsuarioInput} className="form-control" id="documentoUsuario" onChange={(event)=>setDocumentoUsuario(event.target.value)}/>
            </div>
            <div className="mb-3">
              <label htmlFor="adicional" className="form-label">Adicional:</label>
              <input type="text" value={adicionalInput} className="form-control" id="adicional" onChange={(event)=>setAdicional(event.target.value)}/>
            </div>
            <div className="mb-3 form-check">
              <input className="form-check-input" type="checkbox" value={finalizadoInput} id="finalizadoInput" checked={finalizadoInput} onChange={manejadorFinalizado}></input>
              <label className="form-check-label" htmlFor="finalizadoInput">
                ¿El servicio ha Finalizado?
              </label>
            </div>
            <div className="mb-3">
              <label htmlFor="fecha" className="form-label">Fecha:</label>
              <input type="text" value={fechaInput} onChange={(event)=>setFecha(event.target.value)} className="form-control" id="fecha"/>
            </div>
            <div className="mb-3">
              <label htmlFor="hora" className="form-label">Hora:</label>
              <input type="text" value={horaInput} onChange={(event)=>setHora(event.target.value)} className="form-control" id="hora"/>
            </div>
            <button type="submit" className="btn btn-primary fs-3"><i className="fa-solid fa-circle-check"></i> Actualizar</button>
          </form>
        </div>
        <div className='col-4'></div>
      </div>
    </div>
    </>
  )
}

export default ComponenteActualizarServicio;