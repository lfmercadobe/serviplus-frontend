import axios from 'axios';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const URI = 'http://localhost:4000';
const ENDPOINT_LISTAR_SERVICIOS = '/listarServicios/';
const ENDPOINT_LISTAR_FECHAS = '/listarFechas'
const ENDPOINT_ELIMINAR_SERVICIO = '/eliminarServicio/'
let fechaSeleccionada = '21-11-2022';

const ComponenteListarServicios = () => {
  const [servicios, setServicios] = useState([]);
  const [fechas, setFechas] = useState([]);
  useEffect(() => {
    obtenerFechas();
    obtenerServicios('21-11-2022');
  }, []);

  const obtenerServicios = async (fecha) => {
    const respuesta = await axios.get(URI+ENDPOINT_LISTAR_SERVICIOS+fecha);
    fechaSeleccionada = fecha;
    setServicios(respuesta.data.servicios);   
  }

  const obtenerFechas = async () => {
    const respuesta = await axios.get(URI+ENDPOINT_LISTAR_FECHAS);
    setFechas(respuesta.data.fechas);
  }

  const eliminarServicio = async (id) => {
    // eslint-disable-next-line no-restricted-globals
    if(!confirm('¿Seguro que desea eliminar este Servicio?')){
      return;
    }
    const respuesta = await axios.delete(URI+ENDPOINT_ELIMINAR_SERVICIO+id);
    alert(respuesta.data.respuesta);
    obtenerServicios(fechaSeleccionada);
  }

  return(
    <>
    <div className='container mt-4'>
      <div className='row align-items-center'>
        <div className='col-4'></div>
        <div className='col-1 fs-3'>
          <label>Fecha:</label>
        </div>
        <div className='col-2'>
          <select className="form-select fs-5" id="seletFechas" onChange={(event)=>obtenerServicios(event.target.value)}>
            {fechas.map(fecha => (
              <option key={fecha} value={fecha}>{fecha}</option>
            ))}
          </select>
        </div>
      </div>
    </div>
    <hr></hr>
    <div className='container mt-4'>
      <div className='row'>
      <table className="table table-striped">
        <thead className='table-primary'>
          <tr className='fs-5'>
            <th>Tipo Servicio</th>
            <th>Documento Usuario</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
        {servicios.map(servicio => (
            <tr key={servicio._id}>
              <td>{servicio.tipoServicio}</td>
              <td>{servicio.documentoUsuario}</td>
              <td>
                <Link to={`/servicio/${servicio._id}`}>
                  <button type="button" className="btn btn-info me-3">
                    <i className="fa-regular fa-eye"></i>
                  </button>
                </Link>
                <Link to={`/actualizarServicio/${servicio._id}`}>
                  <button type="button" className="btn btn-warning me-3">
                    <i className="fa-solid fa-pen-to-square"></i>
                  </button>
                </Link>
                <Link onClick={() => eliminarServicio(servicio._id)}>
                  <button type="button" className="btn btn-danger me-3">
                      <i  className="fa-solid fa-trash-can text-white"></i>
                  </button>
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      </div>
    </div>
    <hr></hr>
    <div className='container'>
      <div className='row'>
        <div className='col-4'></div>
        <div className='col-4'>
          <Link to={`/crearServicio`}>
            <button type="button" className="btn btn-success fs-4">
              <i className="fa-solid fa-circle-plus"></i> Crear un Nuevo Servicio
            </button> 
          </Link>
        </div>
        <div className='col-4'></div>
      </div>           
    </div>
    </>
  );
}

export default ComponenteListarServicios;