import axios from 'axios';
import { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';

const URI = 'http://localhost:4000/';
const END_POINT_TIPOS_SERVICIOS = 'tipoServicios';
const ENDPOINT_CREAR_SERVICIO = 'crearServicio';
const ENDPOINT_ULTIMO_TICKET = 'ultimoIdTicket';

const ComponenteCrearServicio = () => {

  const [tipoServicioInput, setTipoServicio] = useState('');
  const [documentoUsuarioInput, setDocumentoUsuario] = useState('');
  const [adicionalInput, setAdicional] = useState('');
  const [idTicketInput, setTicket] = useState('');
  const [fechaInput, setFecha] = useState('');
  const [horaInput, setHora] = useState('');
  const [tipoServiciosDB, setTiposServiciosDB] = useState([]);

  const navigate = useNavigate();

  const crearServicio = async(event) => {
    event.preventDefault();
    await axios.post(URI+ENDPOINT_CREAR_SERVICIO,{
      tipoServicio:tipoServicioInput,
      documentoUsuario:documentoUsuarioInput,
      adicional:adicionalInput,
      finalizado:false,
      idTicket:idTicketInput,
      fecha:fechaInput,
      hora:horaInput
    });
    navigate('/')
  }

  useEffect(()=>{
    obtenerTiposServicios();
    obtenerTicketDisponible();
  },[]);

  const obtenerTiposServicios = async()=>{
    const respuesta = await axios.get(URI+END_POINT_TIPOS_SERVICIOS);
    let nombresServicios = respuesta.data.TipoServicios.map((tipoServicio) => tipoServicio.nombre);
    setTiposServiciosDB(nombresServicios);
  }

  const obtenerTicketDisponible = async () => {
    const respuesta = await axios.get(URI+ENDPOINT_ULTIMO_TICKET);
    setTicket(respuesta.data.idTicket+1);
  }


  return(
    <>
    <h1 className='text-center m-4'>CREAR UN NUEVO SERVICIO</h1>
    <div className='container'>
      <div className='row'>
        <div className='col-4'></div>
        <div className='col-4 fs-3'>
          <form onSubmit={crearServicio}>
            <div className="mb-3">
              <label htmlFor="tipoServicio" className="form-label">Tipo de Servicio:</label>
              <select value={tipoServicioInput} className="form-select fs-5" id="selectServicio" onChange={(event)=>setTipoServicio(event.target.value)}>
              {tipoServiciosDB.map(tipo => (
              <option key={tipo} value={tipo}>{tipo}</option>
              ))}
              </select>
            </div>
            <div className="mb-3">
              <label htmlFor="documentoUsuario" className="form-label">Documento Usuario:</label>
              <input type="text" value={documentoUsuarioInput} className="form-control" id="documentoUsuario" onChange={(event)=>setDocumentoUsuario(event.target.value)}/>
            </div>
            <div className="mb-3">
              <label htmlFor="adicional" className="form-label">Adicional:</label>
              <input type="text" value={adicionalInput} className="form-control" id="adicional" onChange={(event)=>setAdicional(event.target.value)}/>
            </div>
            <div className="mb-3">
              <label htmlFor="fecha" className="form-label">Fecha:</label>
              <input type="text" value={fechaInput} onChange={(event)=>setFecha(event.target.value)} className="form-control" id="fecha"/>
            </div>
            <div className="mb-3">
              <label htmlFor="hora" className="form-label">Hora:</label>
              <input type="text" value={horaInput} onChange={(event)=>setHora(event.target.value)} className="form-control" id="hora"/>
            </div>
            <button type="submit" className="btn btn-success fs-4"><i className="fa-solid fa-circle-plus"></i> Crear</button>
          </form>
        </div>
        <div className='col-4'></div>
      </div>
    </div>
    </>
  )
}

export default ComponenteCrearServicio;