import './App.css';
import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import ComponenteListarServicios from './componentes/ServiciosListar';
import ComponenteServicioVer from './componentes/ServicioVer';
import ComponenteActualizarServicio from './componentes/ServicioActualizar';
import ComponenteCrearServicio from './componentes/ServicioCrear';

function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route path='/' element={<ComponenteListarServicios/>}/>
      <Route path='/servicio/:id' element={<ComponenteServicioVer/>}/>
      <Route path='/actualizarServicio/:id' element={<ComponenteActualizarServicio/>}/>
      <Route path='/crearServicio' element={<ComponenteCrearServicio/>}/>
    </Routes>
    </BrowserRouter>
  );
}

export default App;
